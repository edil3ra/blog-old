import React from "react"
import PropTypes from "prop-types"
import { StaticQuery, graphql} from "gatsby"
import css from '../assets/css/layout.module.scss'
import Header from "./header"

const Layout = ({ children }) => (
  <StaticQuery
    query={graphql`
      query LayouteQuery {
        site {
          siteMetadata {
            title
            social {
              github {
                title
                link
              }
              linkedin {
                title
                link
              }
              bitbucket {
                title
                link
              }
            }
          }
        }
      }
    `}
    render={data => {
      return(
        <div>
          <div className={`${css.myHeader}`}>
            <Header
              siteTitle={data.site.siteMetadata.title}
              social={data.site.siteMetadata.social}
            />
          </div>
          <div className={`${css.mySection}`}>
            <div className={`${css.myMainContent} ${css.container}`}>
              <div className={`${css.content}`}>
                <main >{children}</main>
              </div>
            </div>
          </div>
        </div>
      )
    }}
  />
)

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
