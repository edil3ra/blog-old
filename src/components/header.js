import React from 'react'
import PropTypes from "prop-types"
import Link from 'gatsby-link'
import css from '../assets/css/header.module.scss'

const Header = ({ siteTitle, social }) => {

  let navbarMenu = null

  const handleActiveMenu = (event) => {
    event.preventDefault()
    navbarMenu.classList.toggle(css.isActive)
  }

  return (
    <div className={`${css.myNavbar} ${css.isFixedTop}`}>
      <nav className={`${css.navbar} ${css.container}`}>

        <div className={` ${css.navbarBrand}`}>
          <h1 className={`${css.navbarItem}`}>
            <Link to="/">
              Mindfullcoder
            </Link>
          </h1>
          <button className={`${css.navbarBurger} ${css.burger}`}
                  data-target="navbarBasic"
                  aria-label="menu"
                  aria-expanded="false"
                  onClick={handleActiveMenu}>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
          </button>
        </div>
        
        <div className={`${css.navbarMenu}`}
             id="navbarBasic"
             ref={(element) => { navbarMenu = element; }}>
          <div className={`${css.navbarStart}`}>
            <Link to="/articles"
                  className={`${css.navbarItem}`}
                  activeClassName={`${css.isMenuActive}`}
            >
              <span className={css.icon}>
                <i className={`${css.fa} ${css.faPuzzlePiece}`}></i>
              </span>
              <span>Articles</span>
            </Link>
            <Link to="/tutorials" className={`${css.navbarItem}`}
                  activeClassName={`${css.isMenuActive}`}>
              <span className={css.icon}>
                <i className={`${css.fa} ${css.faFlask}`}></i>
              </span>
              <span>Tutorial</span>
            </Link>
            <Link to="/showcases" className={css.navbarItem}
                  activeClassName={`${css.isMenuActive}`}>
              <span className={css.icon}>
                <i className={`${css.fa} ${css.faTerminal}`}></i>
              </span>
              <span>Showcases</span>
            </Link>
            <Link to="/games" className={css.navbarItem}
                  activeClassName={`${css.isMenuActive}`}>
              <span className={css.icon}>
                <i className={`${css.fa} ${css.faGamepad}`}></i>
              </span>
              <span>Games</span>
            </Link>
          </div>
          <div className={css.navbarEnd}>
            <div className={`${css.navbarItem} ${css.hasDropdown} ${css.isHoverable}`}>
              <button >
                More
                <span className={css.icon}>
                  <i className={`${css.fa} ${css.faAngleDown}`}></i>
                </span>
              </button>
              <div className={css.navbarDropdown}>
                <a href={social.github.link} className={css.navbarItem}>
                  <span className={css.icon}>
                    <i className={`${css.fa} ${css.faGithub}`}></i>
                  </span>
                  <span>{ social.github.title }</span>
                </a>
                <a href={social.bitbucket.link} className={css.navbarItem}>
                  <span className={css.icon}>
                    <i className={`${css.fa} ${css.faBitbucket}`}></i>
                  </span>
                  <span>{ social.bitbucket.title }</span>
                </a>
                <a href={social.linkedin.link} className={css.navbarItem}>
                  <span className={css.icon}>
                    <i className={`${css.fa} ${css.faLinkedin}`}></i>
                  </span>
                  <span>{ social.linkedin.title }</span>
                </a>
                <hr className={css.navbarDivider}/>
                <Link to="/cv" className={css.navbarItem}>
                  <span className={css.icon}>
                    <i className={`${css.fa} ${css.faUser}`}></i>
                  </span>
                  <span>About me</span>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </nav>
    </div>
  )
}


Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}


export default Header
