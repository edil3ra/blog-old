import React from 'react'
import { StaticQuery, graphql } from 'gatsby'

import css from '../assets/css/game.module.scss'
import Layout from '../components/layout'
import SEO from '../components/seo'

const GamesPage = () => (
  <StaticQuery
    query={graphql`
      query GamesQuery {
        file(relativePath: { eq: "game/game.yml" }) {
          childGameYaml {
            games {
              title
              source
              host
              image {
                src
                alt
              }
              description {
                title
                text
              }
            }
          }
        }
      }
    `}
    render={data => {
      const d = data.file.childGameYaml
      const games = d.games
      const projectsR = games.map((game, index) => {
        const headerR = (
          <section className={`${css.columns} ${css.myGameHeader}`}>
            <div className="column is-9">
              <h2 className="subtitle is-2">{game.title}</h2>
            </div>
            <div className="column is-3 has-text-right">
              <a className="tag is-link is-large" href={game.host}>
                <strong>Play</strong>
                <span className={css.icon}>
                  <i className={`${css.fa} ${css.faGamepad}`} />
                </span>{' '}
              </a>
              <a className="tag is-link is-large" href={game.source}>
                <strong>Source</strong>
                <span className={css.icon}>
                  <i className={`${css.fa} ${css.faCode}`} />
                </span>{' '}
              </a>
            </div>
          </section>
        )

        const imageR = (
          <div className={`${css.column} ${css.is6} ${css.myGameImage}`}>
            <img
              alt={game.image.alt}
              src={require(`../assets/images/game/${game.image.src}`)}
            />
          </div>
        )

        const descriptionR = (
          <div className={`${css.column} ${css.is6} ${css.myGameDescription}`}>
            <h3 className="subtitle is-3">{game.description.title}</h3>
            <div dangerouslySetInnerHTML={{ __html: game.description.text }} />
          </div>
        )

        return (
          <div key={index} className={`${css.myGameProject}`}>
            {headerR}
            <div className="columns">
              {imageR}
              {descriptionR}
            </div>
          </div>
        )
      })

      return (
        <Layout>
          <SEO
            title="Home"
            keywords={[`edil3ra`, `Home`, `software developer`]}
          />
          <section className={`${css.columns} ${css.isMultiline}`}>
            <div className="column is-12 container content">
              <h1 className="title is-1">Games</h1>
              {projectsR}
            </div>
          </section>
        </Layout>
      )
    }}
  />
)

export default GamesPage
