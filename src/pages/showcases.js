import React from 'react'
import { StaticQuery, graphql } from 'gatsby'

import css from '../assets/css/showcase.module.scss'
import Layout from '../components/layout'
import SEO from '../components/seo'

const ShowCasesPage = () => (
  <StaticQuery
    query={graphql`
      query ShowcasesQuery {
        file(relativePath: { eq: "showcase/showcase.yml" }) {
          childShowcaseYaml {
            showcases {
              title
              host
              images {
                src
                alt
              }
              descriptions {
                title
                text
              }
            }
          }
        }
      }
    `}
    render={data => {
      const d = data.file.childShowcaseYaml
      const showcases = d.showcases

      const showcasesR = showcases.map((project, index) => {
        const imagesR = project.images.map((image, imageIndex) => (
          <section
            key={imageIndex}
            className={`${css.columns} ${css.myShowcaseImage}`}
          >
            <div className="column is-12">
              <img
                alt={image.alt}
                src={require(`../assets/images/showcase/${image.src}`)}
              />
            </div>
          </section>
        ))

        const descriptionsR = project.descriptions.map(
          (description, descriptionIndex) => (
            <section
              key={descriptionIndex}
              className={`${css.columns} ${css.myShowcaseDescription}`}
            >
              <div className="column is-12">
                <h3 className="subtitle is-3">{description.title}</h3>
                <div dangerouslySetInnerHTML={{ __html: description.text }} />
              </div>
            </section>
          )
        )

        return (
          <div key={index} className={`${css.myShowcaseProject}`}>
            <section className={`${css.columns} ${css.myShowcaseHeader}`}>
              <div className="column is-9">
                <h2 className="subtitle is-2">{project.title}</h2>
              </div>
              <div className="column is-3 has-text-right">
                <a className="tag is-host is-large" href={project.host}>
                  <strong>Host</strong>
                  <span className={css.icon}>
                    <i className={`${css.fa} ${css.faExternalLink}`} />
                  </span>{' '}
                </a>
              </div>
            </section>
            {imagesR}
            {descriptionsR}
          </div>
        )
      })

      return (
        <Layout>
          <SEO
            title="Home"
            keywords={[`edil3ra`, `Home`, `software developer`]}
          />
          <section className={`${css.columns} ${css.isMultiline}`}>
            <div className="column is-12 container content">
              <h1 className="title is-1">Showcases</h1>
              {showcasesR}
            </div>
          </section>
        </Layout>
      )
    }}
  />
)

export default ShowCasesPage
