import React from "react"
import { StaticQuery, graphql, Link } from "gatsby"

import css from '../assets/css/showcase.module.scss'
import Layout from "../components/layout"
import SEO from "../components/seo"

const IndexPage = () => (
  <StaticQuery
    query={graphql`
      query IndexQuery {
        site {
          siteMetadata {
            description
          }
        }
      }
    `}
    render={data => (
      <Layout>
        <SEO title="Home" keywords={[`edil3ra`, `Home`, `software developer`]} />
        <div>
          <section className={`${css.columns} ${css.isMultiline}`}>
            <div className="column is-12 content">
              <div dangerouslySetInnerHTML={{__html: data.site.siteMetadata.description}} />
              <Link to="cv"> CV </Link>
            </div>
          </section>
        </div>
      </Layout>
    )}
  />
)

export default IndexPage
